#!/usr/bin/python
# -*- coding: utf-8 -*-

''' TODO:

		-print street number only if existant
		-css by class
		-

'''
import datetime
import sys
import re
import csv
import math
import urllib 		#resources from urls
import operator
import xml.etree.ElementTree as ET


if len(sys.argv) < 2:
	usage = 'Usage: ./bicing.py "restaurant_query"'
	queryPattern = "\n\tThe query must consist on strings combined with lists, [], to indicate disjunction and tuples, (), for conjunctions"
	queryExample = '\n\ti.e. the query: "['+"'udon', ('restaurant','bar')]"+'" will search for restaurants with name udon or bar and restaurant'
	print usage, queryPattern, queryExample
	exit()
else:
	inputQuery = eval(sys.argv[1])

	print "submited query:", inputQuery

	allStations = []

	def distance_on_unit_sphere(lat1, long1, lat2, long2):
	    # Convert latitude and longitude to 
	    # spherical coordinates in radians.
	    degrees_to_radians = math.pi/180.0
	        
	    # phi = 90 - latitude
	    phi1 = (90.0 - lat1)*degrees_to_radians
	    phi2 = (90.0 - lat2)*degrees_to_radians
	        
	    # theta = longitude
	    theta1 = long1*degrees_to_radians
	    theta2 = long2*degrees_to_radians
	        
	    # Compute spherical distance from spherical coordinates.
	        
	    # For two locations in spherical coordinates 
	    # (1, theta, phi) and (1, theta, phi)
	    # cosine( arc length ) = 
	    #    sin phi sin phi' cos(theta-theta') + cos phi cos phi'
	    # distance = rho * arc length
	    
	    cos = (math.sin(phi1)*math.sin(phi2)*math.cos(theta1 - theta2) + 
	           math.cos(phi1)*math.cos(phi2))
	    arc = math.acos( cos )

	    # Remember to multiply arc by the radius of the earth 
	    # in your favorite set of units to get length.
	    return arc*6371


	#define bicing station class
	class bicingStation:
			
		def __init__(self):
			id = ""
			address = ""
			s_num = "s/n"
			freeslots = 0
			bikes = 0
			dist = 99999


		def add_id(self,ident):
			self.id = ident

		def add_address(self,ad):
			self.address = ad

		def add_streetNumber(self,sn):
			self.s_num = sn

		def add_freeSlots(self, fs):
			self.freeslots = fs

		def add_bikes(self, b):
			self.bikes = b

		def add_latitude(self, lat):
			self.lat = lat

		def add_longitude(self, lon):
			self.lon = lon

		def add_distance(self,d):
			self.dist = d


	###########################################
	#get bicing file
	print "Downloading Bicing Stations"
	sock = urllib.urlopen("http://wservice.viabicing.cat/getstations.php?v=1") 
	xmlSource = sock.read()                            
	sock.close()


	# create XML element from string
	root = ET.fromstring(xmlSource)
	#tree = ET.parse('xmlSource')
	#root = tree.getroot()


	# bicing stations list fill up
	# def indexBicingStations():	
	print "Indexing Bicing Stations"
	for s in root.findall('station'):
		bs = bicingStation()

		bs.add_id(s.find('id').text)
		bs.add_address(s.find('street').text)
		bs.add_streetNumber(s.find('streetNumber').text)
		bs.add_freeSlots(s.find('slots').text)
		bs.add_bikes(s.find('bikes').text)
		bs.add_latitude(s.find('lat').text)
		bs.add_longitude(s.find('long').text)

		allStations.append(bs)
	print "Total Bicing Stations Loaded:", len(allStations)
	# indexBicingStations
	

	def printBicingStations(allStations):
		for s in allStations:
			print "Id Station: ",s.id
			print "Address: ",s.address
			print "Free Slots: ",s.freeslots
			print "Bikes: ",s.bikes
	# printBicingStations(allStations)	


	def search_stations_fslots(lat,lon):
		for s in allStations:
			dist = distance_on_unit_sphere(float(lat),float(lon),float(s.lat),float(s.lon))
			if dist <= 1:
				if int(s.freeslots) > 0:
					s.add_distance(dist)
					l_fs.append(s)
				if int(s.bikes) > 0:
					s.add_distance(dist)
					l_b.append(s)

		l_fs.sort(key=operator.attrgetter('dist'))
		l_b.sort(key=operator.attrgetter('dist'))

		# print "len l_fs ", len(l_fs)
		# print "len l_b ", len(l_b)


	def headerBicingStations(b):
		if b == True:
			s = '<style="border-collapse:collapse;border-spacing:0;border-color:#ccc"><tr><th style="font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f0f0f0;border-top-width:1px;border-bottom-width:1px"><b>Bicing Stations with bikes: '+str(len(l_b))+'</b></th><th style="font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f0f0f0;border-top-width:1px;border-bottom-width:1px"><b>Distance</b></th><th style="font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f0f0f0;border-top-width:1px;border-bottom-width:1px"><b>Address</b></th><th style="font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f0f0f0;border-top-width:1px;border-bottom-width:1px"><b>Bikes</b></th></tr>'
			htmloutput.write(s)
		else:
			s = '<style="border-collapse:collapse;border-spacing:0;border-color:#ccc"><tr><th style="font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f0f0f0;border-top-width:1px;border-bottom-width:1px"><b>Bicing Stations with slots: '+str(len(l_fs))+'</b></th><th style="font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f0f0f0;border-top-width:1px;border-bottom-width:1px"><b>Distance</b></th><th style="font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f0f0f0;border-top-width:1px;border-bottom-width:1px"><b>Address</b></th><th style="font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f0f0f0;border-top-width:1px;border-bottom-width:1px"><b>Slots</b></th></tr>' 
			htmloutput.write(s)

	def show_bicingStations():
		r = ''
		if len(l_fs) != []:
			headerBicingStations(True)
			alternate = False
			for s in l_fs:
				if alternate == True:
					r = '<tr><td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f9f9f9"></td><td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f9f9f9">'+str(s.dist)[:5]+'km</td><td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f9f9f9">'+s.address+" "+str(s.s_num)+'</td><td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f9f9f9">'+str(s.freeslots)+'</td></tr>'
				else:
					r = '<tr><td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff"></td><td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff">'+str(s.dist)[:5]+'km</td><td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff">'+s.address+" "+str(s.s_num)+'</td><td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff">'+str(s.freeslots)+'</td></tr>'
				htmloutput.write(r)
				alternate = not alternate
		else:
			htmloutput.write("Unfortunately, this restaurant does not have any bicing station with available slots within 1km")

		if len(l_b) != []:
			headerBicingStations(False)
			alternate = False
			for s in l_b:
				if alternate == True:
					r = '<tr><td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f9f9f9"></td><td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f9f9f9">'+str(s.dist)[:5]+'km</td><td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f9f9f9">'+s.address+" "+str(s.s_num)+'</td><td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f9f9f9">'+str(s.bikes)+'</td></tr>'
				else:
					r = '<tr><td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff"></td><td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff">'+str(s.dist)[:5]+'km</td><td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff">'+s.address+" "+str(s.s_num)+'</td><td style="font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff">'+str(s.bikes)+'</td></tr>'
				htmloutput.write(r)
				alternate = not alternate
		else:
			htmloutput.write("Unfortunately, this restaurant does not have any bicing station with available bikes within 1km")
		htmloutput.write("</table><br><br>")


	###########################################
	## regular expression compiling
	p = re.compile(sys.argv[1],re.I)


	########################################### 
	##  read restaurants.csv
	f = open('restaurants.csv', 'rb')
	c = csv.reader(f, delimiter='\t')
	c.next()
	

	########################################### 
	##  prepare html file
	filename = "resultQuery_"+str(inputQuery)+"_"+str(datetime.datetime.now())+".html"
	htmloutput = open(filename, 'w')

	def initHTML():
		s = '<html><head><title>Restaurants & Bicing Search Results</title><meta http-equiv="Content-Type" content="txt/html; charset=utf-8"/></head>'
		htmloutput.write(s)
		s = '<body>'
		htmloutput.write(s)

	def createCSS():	
		#CSS Restaurant 
		s = '<style type="text/css">'
		htmloutput.write(s)
		s = '.tg  {border-collapse:collapse;border-spacing:0;border-color:#aabcfe;}'
		htmloutput.write(s)
		s = '.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;border-color:#aabcfe;color:#669;background-color:#e8edff;border-top-width:1px;border-bottom-width:1px;}'
		htmloutput.write(s)
		s = '.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;border-color:#aabcfe;color:#039;background-color:#b9c9fe;border-top-width:1px;border-bottom-width:1px;}'
		htmloutput.write(s)
		s = '.tg .tg-vn4c{background-color:#D2E4FC}'
		htmloutput.write(s)
		s = '</style>'
		htmloutput.write(s)


	def createCSSQuery():
		#CSS Query
		s = '<style type="text/css">'
		htmloutput.write(s)
		s = '.tg {border-collapse:collapse;border-spacing:0;border-color:#ccc;}'
		htmloutput.write(s)
		s = '.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff;}'
		htmloutput.write(s)
		s = '.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f0f0f0;}'
		htmloutput.write(s)
		s = '.tg .tg-spn1{background-color:#f9f9f9;text-align:center}'
		htmloutput.write(s)
		s = '.tg .tg-hgcj{font-weight:bold;text-align:center}</style>'
		htmloutput.write(s)

	def queryToHTML():
		s = '<table class="tg">'
		htmloutput.write(s)		
		s = '<tr><th class="tg-hgcj" colspan="2"><b>Restaurant Bicing Stations Finder</b></th></tr>'
		htmloutput.write(s)	
		s = '<tr><td class="tg-031e"><b>Query Searched</b></td><td class="tg-spn1">'+str(inputQuery)+'</td></tr>'
		htmloutput.write(s) 
		s = '<tr><td class="tg-031e"><b>Bicing Stations</b></td><td class="tg-spn1">425</td></tr>'
		htmloutput.write(s) 
		s = '<tr><td class="tg-031e"><b>Restaurants</b></td><td class="tg-spn1">3603</td></tr></table></br>'
		htmloutput.write(s) 

	def createRestaurantTable(row):
				#table & restaurant name
				s = '<table class="tg"><tr><th class="tg-031e" colspan="4"><b>'+str(row[0])+'</b></th></tr><tr>'
				htmloutput.write(s)

				#address
				s = '<td class="tg-vn4c"><b>Address</b></td><td class="tg-vn4c" colspan="3">'+str(row[1])+'</td></tr>'
				htmloutput.write(s)

				#latitude & longitude
				s = '<tr><td class="tg-031e"><b>Latitude</b></td><td class="tg-031e">'+str(row[2])[:6]+'</td><td class="tg-031e"><b>Longitude</b></td><td class="tg-031e">'+str(row[3])[:6]+'</td></tr>'
				htmloutput.write(s)

				#neighbourhood
				s = '<tr><td class="tg-vn4c"><b>Neighbourhood</td><td class="tg-vn4c" colspan="3">'+str(row[6])+'</td></tr>'
				htmloutput.write(s)
				
				#telephone1
				s = '<tr><td class="tg-031e"><b>Telephone 1</b></td><td class="tg-031e" colspan="3">'+str(row[4])+'</td></tr>'
				htmloutput.write(s)

				#telephone2
				s = '<td class="tg-vn4c"><b>Telephone 2</b></td><td class="tg-vn4c" colspan="3">'
				if isinstance(row[5], int):
					s += str(row[5])+'</td></tr>'
				else:
					s += 'not available</td></tr>'
				htmloutput.write(s)

				#url & end restaurant table
				s ='<tr><td class="tg-031e"><b>webpage</b></td><td class="tg-031e" colspan="3">'
				if row[7] != '':
					s += '<a href="'+ str(row[7])+'">'+str(row[7])+'</a></td></tr></table><br>'
				else:
					s += 'not available</td></tr></table><br>'
				htmloutput.write(s)
	
	def checkQuery(inputQuery, rest):
		if inputQuery == []:
			return False
		elif inputQuery == ():
			return True
		elif isinstance(inputQuery, str):
			p = re.compile(inputQuery, re.I)
			searchObj = re.search(p, rest)
			return searchObj != None
		elif isinstance(inputQuery, list):
			if checkQuery(inputQuery[0], rest) == False:
				return checkQuery(inputQuery[1:], rest)
			else:
				return True
		elif isinstance(inputQuery, tuple):
			if checkQuery(inputQuery[0], rest) ==  True:
				return checkQuery(inputQuery[1:], rest)
			else:
				return False

	initHTML()
	createCSSQuery()
	queryToHTML()
	createCSS()
	print "processing resturants"
	numRest = 0
	for row in c:
		numRest += 1
		if checkQuery(inputQuery, row[0]):
			createRestaurantTable(row)
			s = "<table>"
			htmloutput.write(s)
			l_fs = []
			l_b = []
	 		search_stations_fslots(row[2],row[3])
 			show_bicingStations()

 	s = "</table></body></html>"
 	htmloutput.write(s)
 	htmloutput.close
	f.close()
	print "The results of the query have been saved as "+filename
	exit()