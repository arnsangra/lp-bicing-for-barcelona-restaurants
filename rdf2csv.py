#!/usr/bin/python

import re
import csv
from HTMLParser import HTMLParser

allrest=[]
max_tel = 0
#RE to match telephones
p = re.compile('\+?[0-9]*')

class restaurant:

    def __init__(self):
        nom = ""
        address = "unknown address"
        lat = "unknown latitude"
        lon = "unknown longitude"
        tel1 = "no telephone registered" 
        tel2 = "no telephone registered"     


    def afegir_nom(self,nom):
        self.nom = nom

    def afegir_address(self,address):
        self.address = address
    
    def afegir_lat(self,lat):
        self.lat = lat

    def afegir_lon(self,lon):
        self.lon = lon

    def afegir_tel1(self,tel):
        self.tel1 = tel

    def afegir_tel2(self,tel):
        self.tel2 = tel    

    def afegir_barri(self, neighborhood):
        self.neigh = neighborhood

    def afegir_url(self, url):
        self.url = url
        
# creem una subclasse i sobreescribim el metodes del handler
class MHTMLParser(HTMLParser):

    crest = restaurant()
    ctag = ""
    i = 1

    def handle_starttag(self, tag, attrs):
        self.ctag = tag
        if tag == 'v:vcard':
            self.crest = restaurant()
        elif tag == 'v:url':
            self.crest.afegir_url(attrs [0][1])

    def handle_endtag(self, tag):
        self.ctag = ""
        if tag == 'v:vcard':
            allrest.append(self.crest)
            self.i = 1

    def handle_data(self, data):
        #nom
        if self.ctag == 'v:fn':
            self.crest.afegir_nom(data)
        #address
        elif self.ctag == "v:street-address":
            self.crest.afegir_address(data)
        #latitude
        elif self.ctag == "v:latitude":
            self.crest.afegir_lat(data)
        #longitude
        elif self.ctag == "v:longitude":
            self.crest.afegir_lon(data)
        #telefons:
        elif self.ctag == "rdf:value":
            if p.match(data):
                if self.i == 1:
                    self.crest.afegir_tel1(data)
                    self.i = self.i+1
                else:
                    self.crest.afegir_tel2(data)

        #neighborhood
        elif self.ctag == "xv:neighborhood":
            self.crest.afegir_barri(data)

f = open('restaurants.rdf', 'rb') # obre l'arxiu
rdfSource = f.read()                            
f.close()

print "Parsing RDF file"
parser = MHTMLParser()
parser.feed(rdfSource)

def printRestaurants():
    print "numRestaurants:",len(allrest)
    for r in allrest:
        if hasattr(r, "nom"):
            print r.nom
        if hasattr(r, "address"):
            print r.address
        else:
            print "no address"
        print r.lat
        print r.lon
        if hasattr(r, "tel1"):
            print "tel1: ", r.tel1
        if hasattr(r, 'tel2'):
            print "tel2: ", r.tel2
print "Parsed Restaurants:"
printRestaurants

print "Exporting restaurants to CSV"
def restaurantsToCSV():
    f = open('restaurants.csv', 'w')
    c = csv.writer(f, delimiter='\t')
    c.writerow (["Name","Address","Latitude","Longitude","Telephone1","Telephone2","Neighborhood","URL"])
    for r in allrest:
        c.writerow([r.nom,r.address if hasattr(r, "address") else "",r.lat,r.lon,r.tel1 if hasattr(r, "tel1") else "",r.tel2 if hasattr(r, "tel2") else "",r.neigh if hasattr(r, "neigh") else "",r.url if hasattr(r, "url") else ""])
    f.close()
restaurantsToCSV
print "Restaurants exported to CSV file (restaurants.csv)"